imshow('XZBBR-candela.PNG');
axis on
hold on;
%Xorigin = [633, 512];
origin = [633, 512];
% Anormaly = origin(2)-170;
Xnormaly = origin(2)-78;
%Anormalyshift = Anormaly*0.97;
Xnormalyshift = Xnormaly*1;
%Anormalx = origin(1)- 130;
Xnormalx = origin(1)- 200;
r = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.317731959,...
0.602268041,0.809072165,0.942680412,0.990721649, 1,0.94,0.907628866,0.808041237,0.643917526,0.419793814];
theta = 0:(15*pi/180): 2*pi;
thetas = theta - 16*pi/180;
% Plot cross at row 100, column 50
% plot(origin(1), origin(2), 'r+', 'MarkerSize', 30, 'LineWidth', 2);

[x, y] = pol2cart(theta, r);
y = y*Xnormaly + origin(2);
x = x*Xnormalx + origin(1);
% plot(x, y, 'b.', 'MarkerSize', 30, 'LineWidth', 2);
[xshift, yshift] = pol2cart(thetas, r);
yshift = yshift*Xnormalyshift + origin(2);
xshift = xshift*Xnormalx + origin(1);
plot(xshift, yshift, 'b.', 'MarkerSize', 30, 'LineWidth', 2);

