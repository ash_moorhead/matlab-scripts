function [ output, preInterpGyro, preInterpAcc ] = uniformInterp( input_array )
% takes an array of mixed acceleration and gyro data and 
% linearly interpolates the gyro data to fit onto a uniform 
% smapling grid. unifrom grid with 250 Hz

% initialise arrays (use list in java)
gyro = zeros(round(length(input_array)/2),4);
Acc = zeros(round(length(input_array)/2),4);
output = zeros(round(length(input_array)/2),4);
preInterpGyro = zeros(round(length(input_array)/2),4);
preInterpAcc = zeros(round(length(input_array)/2),4);

%set size of uniform grid
uniGrid = 0.004;

% get the position of the first gyro reading
i=1;
while(input_array(i,1) ~= 71)
    i = i+1;
end
firstGyro = i;
startTime = input_array(firstGyro,2);

% step through the values from first gyro reading
ig = 1;
ia = 1;
for i = firstGyro:length(input_array)
    
   % offset the array to the first reading being t=0 
   input_array(i,2) = input_array(i,2)-startTime;
   
   % seperate the sensor data
   if(input_array(i,1)==71)
       preInterpGyro(ig,1:4)=input_array(i,2:5);
       gyro(ig,1:4)=input_array(i,2:5);
       ig=ig+1;
   else
       preInterpAcc(ia,1:4) = input_array(i,2:5);
       Acc(ia,1:4) = input_array(i,2:5);
       ia=ia+1;
   end  
    
   % set uniform output array time stamping
   output((i-firstGyro)+ 1,1)= (i-firstGyro)*uniGrid;
   
end

% put in the first row t = 0
output(1, 5:7) = gyro(1, 2:4);
output(1, 2:4) = Acc(1, 2:4);

% interpolate each point [0,4,8,12,16] based on ideally 3+ points
% and add into the output array both sensors in each row.
for i = 2:length(gyro)
 
    if (rem(gyro(i,1), uniGrid) == 0)
        %the data is at 4ms sampling already
        output(i, 5:7) = gyro(i,2:4);
    else
        % interpolate
        output(i, 5) = output(i-1, 5)+ uniGrid*((gyro(i,2)-output(i-1,5))/(gyro(i, 1)-output(i-1,1)));
        output(i, 6) = output(i-1, 6)+ uniGrid*((gyro(i,3)-output(i-1,6))/(gyro(i, 1)-output(i-1,1)));
        output(i, 7) = output(i-1, 7)+ uniGrid*((gyro(i,4)-output(i-1,7))/(gyro(i, 1)-output(i-1,1)));
    end
end
   
for i = 2:length(Acc)
    
    if (rem(Acc(i,1), uniGrid) == 0)
        %the data is at 4ms sampling already
        output(i, 2:3) = Acc(i,2:3);
    else
        % interpolate
        output(i, 2) = output(i-1, 2)+ uniGrid*((Acc(i,2)-output(i-1,2))/Acc(i, 1));
        output(i, 3) = output(i-1, 3)+ uniGrid*((Acc(i,2)-output(i-1,2))/Acc(i, 1));
        output(i, 4) = output(i-1, 4)+ uniGrid*((Acc(i,2)-output(i-1,2))/Acc(i, 1));
    end
    
end

end

