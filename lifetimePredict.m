%%
%Author:Cyril Au
%Last modified: 03-Apr-2020
%Email:sau282@aucklanduni.ac.nz
%Auckland Bioengineering Institute, The University of Auckland
%
%This script predicts the time it takes for the internal humidity of an
%enclosure to reach a threshold level, based on Dahan's (Dahan, 2013)
%model.
%
%lifetimePrediction.m

%%
%Notes
%All units are in cm for length and g for mass.

figure
%%
%Pre-allocation
tdays = 400;%model humidity change for x days
time_final = tdays*86400;
time = linspace(0,time_final,tdays+1);
tdays_array = zeros(1,length(time));
threshold = zeros(1,length(time));
RHt = zeros(1,length(time));
RHt_tencer = zeros(1,length(time));
y = zeros(1,length(time));

%%
%Environment conditions
RHi = 30;%25.15; %internal relative humidity in % at t = 0;
RHa = 100; %external relative humidity in %;

%%
%Material properties
%steel
P_glass = 1e-10; %permeability coefficient
D_glass = 1; %diffusion coefficient
S_glass = P_glass/D_glass; %solubility coefficient
%PEEK
% P_glass = 1e-13; %permeability coefficient
% D_glass = 1; %diffusion coefficient
% S_glass = P_glass/D_glass; %solubility coefficient
%e30cl
%% Epoxies- https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=1135193
P_e30cl = 1.79e-9; % permeability coefficient of e30cl epoxy
D_e30cl = 1.08e-8; % diffusion coefficient of e30cl epoxy
S_e30cl = P_e30cl/D_e30cl; % solubility coeffcient of e30cl epoxy
%% Cyrils
P_e30cl1 = 6.337e-6; % permeability coefficient of e30cl epoxy
D_e30cl1 = 4.793e-9; % diffusion coefficient of e30cl epoxy
S_e30cl1 = P_e30cl/D_e30cl; % solubility coeffcient of e30cl epoxy

P_e30cl2 = 2.7e-7; %permeability coefficient of e30cl epoxy
D_e30cl2 = 4.793e-9; %diffusion coefficient of e30cl epoxy
S_e30cl2 = P_e30cl/D_e30cl; %solubility coeffcient of e30cl epoxy
perm_props = [P_e30cl, P_e30cl1, P_e30cl2];
diffuse_props = [D_e30cl, D_e30cl1, D_e30cl2];
%%
for prop = 1:3
    
    P_e30cl = perm_props(prop); % permeability coefficient of e30cl epoxy
    D_e30cl = diffuse_props(prop); % diffusion coefficient of e30cl epoxy
    S_e30cl = perm_props(prop)/diffuse_props(prop); % solubility coeffcient of e30cl epoxy

    %Enclosure material properties
    P_wall = P_glass; %permeability coefficient of package walls
    P_seal = P_e30cl; %permeability coefficient of sealant
    S_wall = S_glass; %solubility coefficient of package walls
    S_seal = S_e30cl; %solubility coefficient of sealant

    %Package dimensions
    A_wall = 0.3*0.1*pi(); %22.5013-1.8187;%surface area of enclosure
    d_wall = 0.02; %thickness of enclosure wall
    A_front = 0; %1.8187;
    d_front = 0;
    d_seal = 0.03;%0.09;%thickness of glue line
    A_seal = pi()*0.04^2; %(0.02*11.138);%surface area of glue line
    % A_sealfront = (2*0.0148);
    volume = (0.3-2*d_seal)*A_seal; % 4.85625;%5.90074; %volume of enclosure

    %%
    %calculate Rp for each element
    Rp_wall = d_wall/(P_wall*A_wall); %Rp of peek walls
    Rp_seal = d_seal/(P_seal*A_seal); % Rp of glue line
    %Rp_front = d_front/(P_wall*A_front);%Rp of front
    %Rp_sealfront = d_front/(P_seal*A_sealfront);%Rp of front holes

    %calculate Cp for each element
    Cp_wall = (A_wall*S_wall*d_wall)/2; %Cp of enclosure wall
    Cp_seal = (A_seal*S_seal*d_seal)/2; %Cp of enclosure glue line
    Cp_front = 0; %2*pi()*0.8^2;(A_front*S_wall*d_front)/2; %Cp of front
    Cp_sealfront = 0; %(A_sealfront*S_seal*d_front)/2;%Cp of front holes
    Cv = volume; %Cp of enclosure volume

    %equivalent Rp
    Rp_eq = 1/(1/Rp_wall + 1/Rp_seal);

    %equivalent Cp
    Cp_eq = Cp_seal+Cp_wall+Cv; %Cp_wall + Cp_seal + Cv + Cp_front + Cp_sealfront;

    %calculate time constant
    time_constant = Rp_eq * Cp_eq;

    %%
    %calculate relative humidity inside enclosure over time
    for i = 1:length(time)
        RHt(i) = RHi + (RHa - RHi)*(1-exp(-time(i)/time_constant));
        tdays_array(i) = time(i)/86400;
        threshold(i) = 63.2;
    end
   

    %% This section implements the tencer method for modelling the lifetime to
    % compare with the above electrical analagy approximation
    
    time_constant = (volume*d_seal)/(A_seal*P_e30cl);
    
    for i = 1:length(time)
        RHt_tencer(i) = RHi + (RHa - RHi)*(1-exp(-time(i)/time_constant));
    end
    
    plot(tdays_array,RHt_tencer(1,:),'--r');
    plot(tdays_array,RHt(1,:));
    plot(tdays_array,threshold,'--bl');
    hold all
end

%% plot results
xlabel('Time (days)');
ylabel('Internal relative humidity (%)');

