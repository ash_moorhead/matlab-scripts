% Linear Interpolator
% Test script used to call the lnear interpilation function
clear all
% create test data
Data = zeros(100,4);
Data(:,4) = 1:100;
for i = 1:48
Data(i*3,1) = 1;
end
Data2 = interp(Data);