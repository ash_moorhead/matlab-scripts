% This function is designed to interpolate a set of data
% to a set number of points using a cubic spline. A spline is fit to the
% data first, then interpolated to x points and output
% back to the calling m.file as 'newdata'. 

clear all
close all

%presribe the number of new interpolate data points
nInterpolatedPoints = 100;

% read in data from the two OpenSim .mot files
% normal gait (ng) and pole assisted (poles)
[fileName, pathName] = uigetfile('*.mot');
motData_ng = dlmread([pathName fileName],'\t',8,0);

[fileName, pathName] = uigetfile('*.mot');
motData_poles = dlmread([pathName fileName],'\t',8,0);

% Pull out the variables of interest
time_ng = motData_ng(:,1);
time_poles = motData_poles(:,1);

hip_flexion_r_ng = motData_ng(:,8);
hip_flexion_r_poles = motData_poles(:,8);

hip_adduction_r_ng = motData_ng(:,9);
hip_adduction_r_poles = motData_poles(:,9);

hip_rotation_r_ng = motData_ng(:,10);
hip_rotation_r_poles = motData_poles(:,10);

% select the points of interest for your spline interpolations
plot(time_ng,hip_flexion_r_ng); title('Select START and END point for Spline Interpolation'); xlabel('Time (sec)');ylabel('Angle (deg)');
[x_ng,y_ng]=ginput(2);

plot(time_poles,hip_flexion_r_poles); title('Select START and END point for Spline Interpolation'); xlabel('Time (sec)');ylabel('Angle (deg)');
[x_poles,y_poles]=ginput(2);

% create the new time array for interpolation - ng
newTimeInterval_ng = abs(x_ng(2)-x_ng(1))/(nInterpolatedPoints-1);
newx_ng = (x_ng(1):newTimeInterval_ng:x_ng(2))';

% create the new time array for interpolation - poles
newTimeInterval_poles = abs(x_poles(2)-x_poles(1))/(nInterpolatedPoints-1);
newx_poles = (x_poles(1):newTimeInterval_poles:x_poles(2))';

% interpolate using the cubic spline
newData_flexion_ng = spline(time_ng,hip_flexion_r_ng,newx_ng);
newData_flexion_poles = spline(time_poles,hip_flexion_r_poles,newx_poles);

newData_adduction_ng = spline(time_ng,hip_adduction_r_ng,newx_ng);
newData_adduction_poles = spline(time_poles,hip_adduction_r_poles,newx_poles);
  
newData_rotation_ng = spline(time_ng,hip_rotation_r_ng,newx_ng);
newData_rotation_poles = spline(time_poles,hip_rotation_r_poles,newx_poles);

% plot the results
figure
subplot(3,1,1);
plot(newData_flexion_ng); title('Interpolated Data'); xlabel(' Normalised Time'); ylabel('Hip Flexion Moments')
hold on
plot(newData_flexion_poles,'r');
hold on
subplot(3,1,2);
plot(newData_adduction_ng); title('Interpolated Data'); xlabel(' Normalised Time'); ylabel('Hip Adduction Angle (deg)')
hold on
plot(newData_adduction_poles,'r');
hold on
subplot(3,1,3);
plot(newData_rotation_ng); title('Interpolated Data'); xlabel(' Normalised Time'); ylabel('Hip Rotation Angle (deg)')
hold on
plot(newData_rotation_poles,'r');
