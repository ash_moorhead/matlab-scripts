function [ filteredOutput ] = bandpassFilter( data )
% Applies bandpass filter with below params.

order = 10;
low_end = 3;
high_end = 10;
sample_freq = 200;

%design filter
d = fdesign.bandpass('N,F3dB1,F3dB2',order, low_end, high_end, sample_freq);
Hd = design(d,'butter');
%fvtool(Hd) - for seeing the filter response

filteredOutput = filter(Hd,data);


end

