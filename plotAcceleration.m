function [] = plotAcceleration( Acc )
%Plots data from a matrix with AccX, AccY, AccZ and time

subplot(3,1,1)
plot(Acc(:,1),Acc(:,2));
subplot(3,1,2)
plot(Acc(:,1),Acc(:,3));
subplot(3,1,3)
plot(Acc(:,1),Acc(:,4));

end

