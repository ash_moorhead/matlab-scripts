stage = {"intial", "mid", "end"};
x = 1:3;
data = [3.63029E-05, 2.92E-05, 3.63029E-05, 2.92E-05;
    2.74462E-05,	2.27E-05,	9.59E-06,	8.01E-06;
    2.57084E-05,	2.13E-05,	6.98E-06,	5.84E-06];
xsense = [2,3];
xmost = [1,3];

figure
a = plot(x, data(:,[1,3]), 'r', 'LineWidth', 1);
a(1).Annotation.LegendInformation.IconDisplayStyle = 'off';
set(gca,'XTick',1:3,'XTickLabel',stage)
hold on;
b = plot(x, data(:, [2,4]), 'b', 'LineWidth', 1);
b(1).Annotation.LegendInformation.IconDisplayStyle = 'off';
set(gca,'XTick',1:3,'XTickLabel',stage)
x2 = [x, fliplr(x)];
inBetween = [data(:,1)', fliplr(data(:, 3)')];
least  = fill(x2, inBetween, 'r');
least.FaceAlpha = 0.2;
least.Annotation.LegendInformation.IconDisplayStyle = 'off';
inBetween = [data(:,2)', fliplr(data(:, 4)')];
most  = fill(x2, inBetween, 'b');
most.FaceAlpha = 0.2;
most.Annotation.LegendInformation.IconDisplayStyle = 'off';

leastchange = scatter(xsense, data([2,3],2), 'bx');
mostchange = scatter(xmost, data([1,3],3), 'rx');

ylim([0 4e-5]);
xlim([1 3]);
title(['The effect of encapsulation tissue properties', ' on Irradiance'])
xlabel('Irradiance at detector surface(mW/mm^2)')
ylabel('Tissue growth stage')
legend('least attenuating SCG', 'most attenuating SCG', 'least change', 'most change')