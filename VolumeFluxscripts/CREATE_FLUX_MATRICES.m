% Script file to load results files from TracePro and combine into matrices
% of flux vectors. Saves results as a .mat file to be processed by other
% scripts. 

% Individual datafiles contain data for 1 million rays
% traced. Datafiles are combined sequentially to build a dataset of 1e6 up
% to Ne6 rays traced.

% File format consists of one row per cell (voxel), columns are:
%       |  cell centres   |   flux per cell                          |
% cell# |  X  |  Y  |  Z  | Originating | Incident | Absorbed | Lost |

%% Set up workspace
clear all
close all
clc

reverseStr = ''; % used for UpdateProgess.m function

%% control variable values
%change these values to match the data from your volume flux analysis
%results file
disp("begin summarising raw data")
fluxPerFile = 1e6; %1 million Watts per file (100,000 rays * 10 traces)
nFiles = 10;
orderingVector = 1:nFiles;

% set strings for filenames
%reading
filenamePrefix = "SV";
%writing
tissueType = "Brain";
vectorFile = sprintf("%sFluxVectors.mat", tissueType);
matrixFile = sprintf("%sFluxMatrix.mat", tissueType);

% set system bounds and voxel numbers
nCellsx = 120;
nCellsy = 120;
nCellsz = 100;
bounds = [-6, 6; -6, 6; -4, 6];

% save config data, for use in other processing scripts
configData = struct('nCellsx', nCellsx, 'nCellsy', nCellsy, 'nCellsz', nCellsz, 'bounds', bounds);
save(sprintf("%sConfig.mat", tissueType), 'configData');

% calculation and plotting controls
calculateRelDiff = 1;
calculateAbsDiff = 1;
plotRelDiff = 0;
plotAbsDiff = 0;

%% import data from volume flux analysis files
fluxData = zeros(nFiles, nCellsx * nCellsy * nCellsz, 4);

% vectors of X, Y, and Z centre coordinates for each cell
xCentres = PopulateGridVector(nCellsx, bounds(1, :), 0);
yCentres = PopulateGridVector(nCellsy, bounds(2, :), 0);
zCentres = PopulateGridVector(nCellsz, bounds(3, :), 0);

% generate filenames, import data from text files into matrix
tLoad = tic;
for file = 1:nFiles
    % load file
    filename = sprintf("%s_%03i-%03i.txt", filenamePrefix, file*10-9, file*10);
    fluxData(file, :, :) = GetFluxVectorsFromFile(filename, 13, 12 + nCellsx*nCellsy*nCellsz);
    
    % display progress
    reverseStr = UpdateProgress(reverseStr, toc(tLoad), file, nFiles);
    
    %add line after final UpdateProgress call
    if file == nFiles
        fprintf("\n");
    end
end

% combine individual sets of flux vectors according to the ordering vector,
% and calculate summarised and normalised data. Each row of summarised and
% normalised data corresponds to one set of traced rays.
[summarisedData, normalisedData, combinedFluxData] ...
    = SummariseFluxData(fluxData, orderingVector);
% clear fluxData

%normalise combined flux data
for file = 1:nFiles
    %     combined_flux_data(file, :, :) = 100 .* combined_flux_data(file, :, :) ./ (file*flux_per_file);
    combinedFluxData(file, :, :) = combinedFluxData(file, :, :) ./ (file*fluxPerFile);
end

save(vectorFile, 'combinedFluxData');

%% convert flux vectors into 3D matrix for ease of plotting
absorbedFluxMatrix = zeros(nFiles, nCellsx, nCellsy, nCellsz);
disp("RESHAPING ABSORPTION VECTORS TO MATRICES")
tShape = tic;
for file = 1:nFiles
    absorbedFluxMatrix(file, :, :, :) = reshape(combinedFluxData(file, :, 3), nCellsx, nCellsy, nCellsz);
    reverseStr = UpdateProgress(reverseStr, toc(tShape), file, nFiles);
end
fprintf("\n");

save(matrixFile, 'absorbedFluxMatrix');