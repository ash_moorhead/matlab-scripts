function reverseStr = UpdateProgress(reverseStr, tNow, currentIndex, maxIndex)

percentDone = currentIndex / maxIndex;
tTotal = seconds(tNow) / percentDone;
tRemain = tTotal - seconds(tNow);
tRemain.Format = 'hh:mm:ss';

msg = sprintf('Percent done: %3.1f \t Remaining time: %s', 100*percentDone, tRemain);
fprintf([reverseStr, msg]);
reverseStr = repmat(sprintf('\b'), 1, length(msg));

end