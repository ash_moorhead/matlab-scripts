function vector = PopulateGridVector(size, bounds, offset)
multiplier = (bounds(2) - bounds(1)) / size;
vector = linspace(bounds(1) + 0.5*multiplier + offset, bounds(2) - 0.5*multiplier + offset, size);
end