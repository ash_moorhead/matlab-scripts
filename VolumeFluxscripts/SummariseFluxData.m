function [summarisedData,normalisedData, fluxData] = SummariseFluxData(fluxData, orderingVector)
%SUMMARISE_FLUX_DATA Loads raw flux data from text files and outputs
%a summary of originating, incident, absorbed, and lost flux total from the
%dataset in raw and normalised forms.
%   output argument "fluxData" is the combined form of the individual
%   datasets

% allocate space for raw flux data
nFiles = length(orderingVector);

% allocate space for summarised and normalised data
summarisedData = zeros(nFiles, 4);
normalisedData = zeros(nFiles, 4);
combinedData = zeros(nFiles, size(fluxData, 2), 4);

for file = 1:nFiles
    
    % combine individual datasets according to orderingVector to increase
    % number of rays being analysed
    if file == 1
        % do nothing
        combinedData(file, :, :) = fluxData(orderingVector(file), :, :);
    else
        fluxData(file, :, :) = fluxData(orderingVector(file), :, :)...
            + fluxData(orderingVector(file - 1), :, :);
        
        combinedData(file, :, :) = fluxData(orderingVector(file), :, :)...
            + combinedData(file - 1, :, :);
    end
    
    
    summarisedData(file, :) = sum(fluxData(file, :, :));
    normalisedData(file, :) = summarisedData(file, :) ./ summarisedData(file, 1);
end
end
