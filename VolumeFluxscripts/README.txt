Scripts are saved with filenames in ALL_CAPS, with words separated by underscores. 

Example output files are included:
- exampleConfig
- exampleFluxMatrix
- examplesFluxVectors

Necessary functions are included. Filenames are in camelCase but with first letters capitalised:
- GetFluxVectorsFromFile
- PopulateGridVector
- SummariseFluxData
- UpdateProgress

Run the script CREATE_FLUX_MATRICES first. You'll need to modify some lines to read in your volume flux files properly. 

There is a variable for how many rays were traced for each raw data file, currently set to 1 million.
The first script creates large arrays of combined flux files. E.g. if you have 10 raw data files with the volume flux results
from 1 million rays in each file, the script will create flux vector and flux matrix files with 1e6, 2e6, 3e6, 4e6, ..., 10e6 rays included.
Data in these files is normalised against the total rays traced. 

Run the script CONVERGENCE_ANALYSIS second. You'll need to modify the config and data files for it to read. 
This script calculates relative and absolute differences between every voxel of the flux vectors/matrices output from the previous script.
These differences are plotted together, with the mean value across all values plotted as a line.
With 10 raw data files, 9 plots will be created, showing differences between 2e6 - 1e6 rays, 3e6 - 2e6, ..., 10e6 - 9e6. 

Convergence is visualised by:
- finding absolute difference between every voxel e.g. for 2e6 - 1e6, 3e6 - 2e6, ..., 10e6 - 9e6 rays
- Summing the absolute difference for all voxels
- plotting the sum of absolute differences as a percentage of total energy. 