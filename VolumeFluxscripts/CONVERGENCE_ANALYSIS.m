% Script file to find number of rays to trace for Safety Study simulations
% to be confident that the solution has converged. The solution of interest
% is the distribution of absorbed flux in the volume flux analysis.

%% Set up workspace
clear all
close all
clc

%% load data to analyse
filename = "exampleFluxVectors";
cfgFile = "exampleConfig";
extension = ".mat";
combinedFluxData = load(sprintf('%s%s', filename, extension));
combinedFluxData = combinedFluxData.combinedFluxData;

cfg = load(sprintf('%s%s', cfgFile, extension));
cfg = cfg.configData;

nFiles = size(combinedFluxData, 1);

%% calculate relative and absolute differences
plotRelDiff = 1;
plotAbsDiff = 1;
for file = 1:nFiles
    
    %relative differences
    if file == 1
        relDiff = zeros(cfg.nCellsx * cfg.nCellsy * cfg.nCellsz, nFiles - 1);
        relDiffSummary = zeros(nFiles - 1, 4);
    else
        relDiff(:, file - 1) = abs(combinedFluxData(file, :, 3) - combinedFluxData(file - 1, :, 3)) ./ combinedFluxData(file - 1, :, 3);
        relDiff(isnan(relDiff)) = 0;
        relDiff(isinf(relDiff)) = 0;
        relDiffSummary(file - 1, 1) = mean(relDiff(:, file - 1));
        relDiffSummary(file - 1, 2) = min(relDiff(:, file - 1));
        relDiffSummary(file - 1, 3) = max(relDiff(:, file - 1));
        relDiffSummary(file - 1, 4) = sum(relDiff(:, file - 1));
        
        if plotRelDiff == 1 && plotAbsDiff == 0
            figure
            hold on
            plot(relDiff(:, file - 1), 'x');
            yline(0);
            yline(mean(relDiff(:, file - 1)), 'r');
            titleString = sprintf("%ie6 minus %ie6 rays", file, file - 1);
            title(titleString);
            xlabel('voxel');
            ylabel('relative difference')
        end
    end
    
    %absolute differences
    if file == 1
        absDiff = zeros(cfg.nCellsx * cfg.nCellsy * cfg.nCellsz, nFiles - 1);
        absDiffSummary = zeros(nFiles - 1, 5);
    else
        absDiff(:, file - 1) = abs(combinedFluxData(file, :, 3) - combinedFluxData(file - 1, :, 3));
        absDiff(isnan(absDiff)) = 0;
        absDiff(isinf(absDiff)) = 0;
        absDiffSummary(file - 1, 1) = mean(absDiff(:, file - 1));
        absDiffSummary(file - 1, 2) = min(absDiff(:, file - 1));
        absDiffSummary(file - 1, 3) = max(absDiff(:, file - 1));
        absDiffSummary(file - 1, 4) = sum(absDiff(:, file - 1));
        absDiffSummary(file - 1, 5) = median(absDiff(:, file - 1));
        
        if plotRelDiff == 0 && plotAbsDiff == 1
            figure
            hold on
            plot(absDiff(:, file - 1), 'x');
            yline(0);
            yline(mean(absDiff(:, file - 1)), 'r');
            titleString = sprintf("%ie6 minus %ie6 rays", file, file - 1);
            title(titleString);
            xlabel('voxel');
            ylabel('absolute difference')
        end
    end
    
    if plotRelDiff == 1 && plotAbsDiff == 1
        if file ~= 1
        figure
        hold on
        titleString = sprintf("%ie6 minus %ie6 rays", file, file - 1);
        suptitle(titleString);
        
        subplot(1, 2, 1);
        plot(relDiff(:, file - 1), 'x');
        yline(0);
        yline(mean(relDiff(:, file - 1)), 'r');
        xlabel('voxel');
        ylabel('relative difference')
        
        subplot(1, 2, 2);
        plot(absDiff(:, file - 1), 'x');
        yline(0);
        yline(mean(absDiff(:, file - 1)), 'r');
        xlabel('voxel');
        ylabel('absolute difference')
        end
    end
end

%% plot convergence results

figure
plot(100 .* absDiffSummary(:, 4) ./ sum(combinedFluxData(2:nFiles, :, 3), 2));
xlabel('Dataset');
ylabel('% Difference');
title('Sum of Element-wise Absolute Differences as Percentage of Total Energy in Model"');