function [ filtered_data ] = highpassFilter( data )
% Filters the data on predefined cut-off etc.

Fstop = 5;
Fpass = 10;
Astop = 65;
Apass = 0.5;
Fs = 500;

d = designfilt('highpassfir','StopbandFrequency',Fstop, ...
  'PassbandFrequency',Fpass,'StopbandAttenuation',Astop, ...
  'PassbandRipple',Apass,'SampleRate',Fs,'DesignMethod','equiripple');

fvtool(d)

filtered_data = step(d, data);
end

