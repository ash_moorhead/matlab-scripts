function [ D ] = interp( D )
%This function takes in the raw data and interpilates the missing points

s = size(D);

% place in safety catch
D(1,:) = 1;
D(s(1),:) = 1;

% Nested loops to cycle through each point
for i = 4:s(1)-4
    for j = 1:s(2)-1
        
        % Determine if point is empty
        create = 0;
        if D(i,j) == 0
            create = 1;
        end
        
        if create % for single missing point todo add last known data point
            k = 0;
            g = 0;
            looking = 1;
            looking2 = 1;
            %find previous time point of last known data point
            while looking
                k = k + 1;
                if D(i-k,j) ~= 0
                    previoustime = D(i-k,s(2));
                    lastdatapoint = D(i-k,j);
                    looking = 0;
                end
            end
            % find next time point of last know data point
            while looking2
                g = g + 1;
                if D(i+g,j) ~= 0
                    nexttime = D(i+g,s(2));
                    nextdatapoint = D(i+g,j);
                    looking2 = 0;
                end
            end
            
            timestep = nexttime - previoustime;
            currenttime = D(i,s(2));
            %Calculate ratio
            x = (currenttime - previoustime)/timestep;
            
            datastep = nextdatapoint - lastdatapoint;
            
            newpoint = lastdatapoint + datastep*x;
            D(i,j) = newpoint;
            
        end
    end
end

% clean up Data

D = D([5:s(1)-5],[1:4]);

end

