function [ filteredData ] = lowpassFilter( data)
% this takes in data and filters it based on a predefined 
% cut-off frequency etc.

N   = 150;      % order of filter
Fs  = 10e3;       % sampling frequency
Fp  = 2;        % edge of passband (Hz) 
Ap  = 0.2;
Ast = 100;       % attenuation

Rp  = (10^(Ap/20) - 1)/(10^(Ap/20) + 1);
Rst = 10^(-Ast/20);

NUM = firceqrip(N,Fp/(Fs/2),[Rp Rst],'passedge');
%fvtool(NUM,'Fs',Fs)

LP_FIR = dsp.FIRFilter('Numerator',NUM);
filteredData = step(LP_FIR,data);

end

