function [ Acc ] = readData()
% readData reads data from a text file in the form:
%    AccX:AccY:AccZ:time
% input: text file
% output: array of values

filename = uigetfile('.txt');

if (filename == 0)
    disp('a file needs to be chosen')
    return
end

fid = fopen(filename, 'r');

raw = fscanf(fid, '%f:%f:%f:%f');
fclose(fid);
Acc = zeros(length(raw)/4, 4);

j = 1:4:length(raw);
for i = 1:(length(raw)/4)

    Acc(i, 1) = raw(j(i));
    Acc(i, 2) = raw(j(i)+1);
    Acc(i, 3) = raw(j(i)+2);
    Acc(i, 4) = (raw(j(i)+3))/1000;

end

end

