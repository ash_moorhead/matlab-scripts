function [Acc] = readDat()

% readDat reads data from a text file in the form:
%    AccX:AccY:AccZ:time
% input: text file
% output: array of values
clear all;
filename = uigetfile('.txt');

if (filename == 0)
    disp('a file needs to be chosen')
    return
end

fid = fopen(filename, 'r');

raw = fscanf(fid, '%c%f\t%f\t%f\t%f\n');
fclose(fid);
Acc = zeros(length(raw)/5, 5);

j = 1:5:length(raw);
for i = 1:(length(raw)/5)

    Acc(i, 1) = raw(j(i));
    Acc(i, 2) = raw(j(i)+1)/1000;
    Acc(i, 3) = raw(j(i)+2);
    Acc(i, 4) = raw(j(i)+3);
    Acc(i, 5) = raw(j(i)+4);
    %Acc(i, 6) = raw(j(i)+5);
    %Acc(i, 7) = raw(j(i)+6);

end
end

