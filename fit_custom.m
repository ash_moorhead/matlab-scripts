x = [1.124323142,1.038363343,0.866825228,0.4, 0.2, 0.05];
T = [0.0267772027258526, 0.028568, 0.037668, 0.09757973, 0.219696968, 0.392699203];

xplant = [ 1.39585134];
Tplant= [ 0.005 ];
% Mouse	
	
xM = [0.20021,0.30079, 0.40145, 1.00164];
TM = [0.32514, 0.21032, 0.15794, 0.06284];
	
% Rat	
xR= [0.30047, 0.39939, 0.50276, 0.69939, 0.99887];	
TR = [0.27282, 0.21831, 0.17167, 0.11917, 0.11159];

error_bars1 = [0.5*T(2),0.5*T(3), 0.5*T(4), 0.05536873, 0.0384185834348906, 0.033181818, 0.0025]; 

error_bars = [0.00582784122685481, 0.00926647260926741, 0.0184185834348906, 0.05536873, 0.0384185834348906, 0.033181818]; 
tbl = table(x', T');
% Define Start points, fit-function and fit curve
modelfun = @(c,x) 1./(1+c(2).*x);  
%modelfun = @(c,x) exp(-x./c(2));  
beta0 = [1, 5]; % Guess values to start with.  Just make your best guess.
% Now the next line is where the actual model computation is done.
mdl = fitnlm(tbl, modelfun, beta0); 

% Extract the coefficient values from the the model object.
% The actual coefficients are in the "Estimate" column of the "Coefficients" table that's part of the mode.
coefficients = mdl.Coefficients{:, 'Estimate'}
% Create smoothed/regressed data using the model:
xFit = linspace(0, 1.5, 500);
yFitted = 1./(1+coefficients(2) .* xFit);
%yFitted = coefficients(1) * exp(-xFit./coefficients(2));
% Now we're done and we can plot the smooth model as a red line going through the noisy blue markers.
figure
plot(xFit, yFitted, 'b-');
hold on
scatter(x, T, 'b')
scatter(xplant , Tplant, 'r')
scatter(xM, TM, 'rx')
scatter(xR, TR, 'gx')
errorbar(cat(2, x,  xplant), cat(2, T, Tplant), error_bars1, 'LineStyle','none');
%errorbar(1.39585134, 0.005, 0,0025,'LineStyle','none');
ylim([0 1]);
xlim([0 1.5]);
xlabel('Sample thickness (mm)')
ylabel('Transmission Fraction')
legend('Kubelka-munk fit', 'SCG measurements', 'Day 5 Measurement', 'Aravanis et al. (rat brain)', 'Aravanis et al. (murine brain)', '1 SD')




