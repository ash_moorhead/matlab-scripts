%% Read in raw data from text file

clear all;
clear;
clc;
rawData = csvread('run54.txt');
N = 30;
delay = round(N/2);
Roll = rawData(:,7);
Pitch = rawData(:,8);
Yaw = rawData(:,9);
Rollx = zeros(length(Roll)+delay, 1);
Pitchy = zeros(length(Roll)+delay, 1);
Yawz = zeros(length(Roll)+delay, 1);

for i = 1:length(Roll)-delay
   
    Rollx(i+delay) = Roll(i);
    Pitchy(i+delay) = Pitch(i);
    Yawz(i+delay) = Yaw(i);
       
end

time = 0:(length(Roll)-1);


[ filteredDataX, fDelayX ] = magnoFilter( N , Roll);
[ filteredDataY, fDelayY ] = magnoFilter( N , Pitch);
[ filteredDataZ, fDelayZ ] = magnoFilter( N , Yaw);

Rollx(314) = -5;
subplot(3, 1, 1)
plot(Rollx);
hold on
plot(filteredDataX, 'r'); 
subplot(3, 1, 2)
plot(Pitchy);
hold on
plot(filteredDataY, 'r');
subplot(3, 1, 3)
plot(Yawz);
hold on
plot(filteredDataZ, 'r');

