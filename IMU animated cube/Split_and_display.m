
%% Read in raw data from text file

clear all;
clc;
rawData = csvread('run54.txt');

accX = rawData(:,1);
accY = rawData(:,2);
accZ = rawData(:,3);
gyroX = rawData(:,4);
gyroY = rawData(:,5);
gyroZ = rawData(:,6);
Roll = rawData(:,7);
Pitch = rawData(:,8);
Yaw = rawData(:,9);

subplot(3, 1, 1)
plot(accX);
subplot(3, 1, 2)
plot(accY);
subplot(3, 1, 3)
plot(accZ);

figure(2)
subplot(3, 1, 1)
plot(Roll);
subplot(3, 1, 2)
plot(Pitch);
subplot(3, 1, 3)
plot(Yaw);

figure(3)
subplot(3, 1, 1)
plot(gyroX);
subplot(3, 1, 2)
plot(gyroY);
subplot(3, 1, 3)
plot(gyroZ);

gx = 9.81.*sind(Pitch);
gy = 9.81.*sind(Roll);


noGravityX = accX + gx;

noGravityY = accY - gy;

noGravityZ = accZ - (9.81 - (abs(gx)+ abs(gy)));


max(accX)
max(noGravityX)
max(accY)
max(noGravityY)
max(accZ)
max(noGravityZ)


figure(4)
subplot(3, 1, 1)
plot(noGravityX);
subplot(3, 1, 2)
plot(noGravityY);
subplot(3, 1, 3)
plot(noGravityZ);
