function Tnew = ImprovedEulerStep(Told, dt, A, B, C, Sin)
% calls the slope function from Task 2.2c    
    function dTdt = f(T)
        dTdt = slope(T, A, B, C, Sin);
    end

% Evaluates initial slope by calling the in-built function f(T)
dTdt = f(Told);
% Evaluates predictor formula using formula from 2.2a
T1 = Told +(dt*dTdt);
% Evaluates predicted slope using f(T) with the predicted Temp T1
predTdt = f(T1);
% Takes Improved Euler step using intermediate quantities
Tnew = Told + ((0.5*dt)*(dTdt + predTdt));
end


