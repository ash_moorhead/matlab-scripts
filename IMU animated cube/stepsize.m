T0 = 10; tf = 100; A = 2.17; B = 204; C = 17; Sin = 1500; % parameters
numSteps = 1; % Initial number of steps
tol = 1e-4; % Required accuracy
% Convergence analysis ("numSteps doubling")
Tf = ImprovedEuler(T0, tf, numSteps, A, B, C, Sin);
while true
TfDoubled = ImprovedEuler(T0, tf, 2*numSteps, A, B, C, Sin);
absDiff = abs(Tf - TfDoubled);
fprintf('Doubling on %d steps: %1.1e\n',numSteps, absDiff)
% if tolerance is met, breaks out of the loop
if absDiff <= tol
    break
end
% Updates Tf and numSteps for the next loop
Tf = TfDoubled;
numSteps = 2*numSteps;
end

fprintf('Final Temperature = %g\n', Tf)
