function Tf = ImprovedEuler(T0, tf, numSteps, A, B, C, Sin)
% Determines time step by dividing total time by number of steps
dt = tf/numSteps;
% Loop over a sequence of Improved Euler steps until number of steps is reached
n = 0;
Tf = T0;
 
% This sets the condition so that when the total number of steps 
% have been completed the while loop is exited
while n < numSteps           
   n =  n + 1;  %updates total steps completed
% This calls the function improved euler which contains the algorithm to calculate the corrector and predictor steps 
Tf = ImprovedEulerStep(Tf, dt, A, B, C, Sin);
end

end
