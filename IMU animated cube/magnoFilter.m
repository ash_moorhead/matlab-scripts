function [ filteredData, fDelay ] = magnoFilter( N , MagnoData)
%Magno Filter 
%uses moving average of N points.

Coeffs = ones(1, N)/N;
filteredData = filter(Coeffs, 1, MagnoData);

fDelay = (length(Coeffs)-1)/2;

end