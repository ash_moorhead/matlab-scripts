function [ CompensatedData ] = gravityCompensate( rawData )
%GRAVITYCOMPENSATE removes the gravity component of the data using a
%quarternion algebraic method. Must input 3Magno axis and 3Acceleration
%axis of Data. Outputs 3 compensated Acceleration axis.

accX = rawData(:,1);
accY = rawData(:,2);
accZ = rawData(:,3);
Roll = rawData(:,7);
Pitch = rawData(:,8);
Yaw = rawData(:,9);

Angles = [Roll,Pitch,Yaw];
q=zeros(4);
noGravityX = zeros(length(accX), 1);
noGravityY = zeros(length(accX), 1);
noGravityZ = zeros(length(accX), 1);

for i = 1:length(Roll)
    
    q(1)=(cosd(Angles(i,1)/2)*cosd(Angles(i,2)/2)*cosd(Angles(i,3)/2))+(sind(Angles(i,1)/2)*sind(Angles(i,2)/2)*sind(Angles(i,3)/2));
    q(2)=(sind(Angles(i,1)/2)*cosd(Angles(i,2)/2)*cosd(Angles(i,3)/2))-(cosd(Angles(i,1)/2)*sind(Angles(i,2)/2)*sind(Angles(i,3)/2));
    q(3)=(cosd(Angles(i,1)/2)*sind(Angles(i,2)/2)*cosd(Angles(i,3)/2))+(sind(Angles(i,1)/2)*cosd(Angles(i,2)/2)*sind(Angles(i,3)/2));
    q(4)=(cosd(Angles(i,1)/2)*cosd(Angles(i,2)/2)*sind(Angles(i,3)/2))-(sind(Angles(i,1)/2)*sind(Angles(i,2)/2)*cosd(Angles(i,3)/2));
    
    g = [0, 0, 0];
    
    g(1) = 2 * (q(2) * q(4) - q(1) * q(3));
    g(2) = 2 * (q(1) * q(2) + q(3) * q(4));
    g(3) = q(1) * q(1) - q(2) * q(2) - q(3) * q(3) + q(4) * q(4);
    
    noGravityX(i) = accX(i) - 10*g(1);
    noGravityY(i) = accY(i) - 10*g(2);
    noGravityZ(i) = accZ(i) - 10*g(3);
    
    
end
CompensatedData = [noGravityX, noGravityY, noGravityZ];

end

