% sets the initial conditions for the slope to be calculated from
tf = 100; A = 2.17; B = 204; C = 17; Sin = 1500;
% sets an array of all the values between - 40 and 40 
% in order to find the tipping point
To = -40:0.1:40; 

% this for loop steps through all the elements of the array unless it 
% reaches a break              
for i = 1:length(To)
T = ImprovedEuler(To(i), 1, 50, 2.17, 204, 17, 1500);
% this if statement stores T2 as T if T is less than -23 i.e. the lower
% steady state
if T < -23
   T2 = T;
 %  else T is stored as T1 i.e upper steady state
elseif T > -23
   T1 = T;
end
% this condition calculates the abs difference between T2( the value we get
% first as we step through the array from the bottom.)and T and if it is to
% far from zero it is an indication that the tipping point has been reached
% and so Tc stores the value from the array.
if ((-0.5 < abs(T) - abs(T2)) && (abs(T) - abs(T2) < 0.5)) 
else
    Tc = To(i);
    break % we now have all the required outputs so we break out of the loop
end

end

