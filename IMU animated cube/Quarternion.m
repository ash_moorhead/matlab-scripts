% calculate the quarternion from the magno data
%% Read in raw data from text file

clear all;
clc;
rawData = csvread('run61.txt');

accX = rawData(:,1);
accY = rawData(:,2);
accZ = rawData(:,3);
Roll = rawData(:,7);
Pitch = rawData(:,8);
Yaw = rawData(:,9);

Angles = [Roll,Pitch,Yaw];
q=zeros(4);
noGravityX = zeros(length(accX), 1);
noGravityY = zeros(length(accX), 1);
noGravityZ = zeros(length(accX), 1);

for i = 1:length(Roll)
    
    q(1)=(cosd(Angles(i,1)/2)*cosd(Angles(i,2)/2)*cosd(Angles(i,3)/2))+(sind(Angles(i,1)/2)*sind(Angles(i,2)/2)*sind(Angles(i,3)/2));
    q(2)=(sind(Angles(i,1)/2)*cosd(Angles(i,2)/2)*cosd(Angles(i,3)/2))-(cosd(Angles(i,1)/2)*sind(Angles(i,2)/2)*sind(Angles(i,3)/2));
    q(3)=(cosd(Angles(i,1)/2)*sind(Angles(i,2)/2)*cosd(Angles(i,3)/2))+(sind(Angles(i,1)/2)*cosd(Angles(i,2)/2)*sind(Angles(i,3)/2));
    q(4)=(cosd(Angles(i,1)/2)*cosd(Angles(i,2)/2)*sind(Angles(i,3)/2))-(sind(Angles(i,1)/2)*sind(Angles(i,2)/2)*cosd(Angles(i,3)/2));
    
    g = [0, 0, 0];
    
    g(1) = 2 * (q(2) * q(4) - q(1) * q(3));
    g(2) = 2 * (q(1) * q(2) + q(3) * q(4));
    g(3) = q(1) * q(1) - q(2) * q(2) - q(3) * q(3) + q(4) * q(4);
    
    noGravityX(i) = accX(i) - 10*g(1);
    noGravityY(i) = accY(i) - 10*g(2);
    noGravityZ(i) = accZ(i) - 10*g(3);
    
    
end
subplot(3, 1, 1)
plot(accX);
subplot(3, 1, 2)
plot(accY);
subplot(3, 1, 3)
plot(accZ);

figure(2)
subplot(3, 1, 1)
plot(noGravityX);
subplot(3, 1, 2)
plot(noGravityY);
subplot(3, 1, 3)
plot(noGravityZ);
