function [dTdt] = slope(T, A, B, C, Sin)
 
G = 1;
alpha = 0.5-(0.2*tanh((T+23)/10)); 
 
dTdt = ((Sin *(1-alpha))-(4*G*(A*T+B)))/4*C;
 
 
end



